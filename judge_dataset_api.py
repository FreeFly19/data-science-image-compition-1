import json
import sys
from os.path import exists

# if you have an error here, you need to install 'requests' library:
# pip install requests
import requests
import pandas as pd

host = 'http://leaderboard.track-debts.com'


def get_test_data():
    if not exists('test-data.json'):
        response = requests.get(host + '/data')
        assert response.status_code == 200, 'An error occurred during dataset downloading, ask Oleksandr Melnyk for help'

        with open('test-data.json', 'w') as f:
            json.dump(response.json(), f)

    return pd.read_json('test-data.json').values


def submit_predictions(y_predicted, user_name):
    with open(sys.argv[0], 'r') as code_file:
        code = code_file.read()
    response = requests.post(host + '/submit', json={
        'user_name': user_name,
        'code': code,
        'predictions': list(map(int, y_predicted))
    })
    assert response.status_code == 200, 'An error occurred, ask Oleksandr Melnyk for help'

    return response.json()['score']

