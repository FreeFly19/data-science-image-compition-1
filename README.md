# Welcome to your first Data Science Competition

## Installation
1. Download data from this repo as an `zip` archive (or with `git clone` of you know how).
2. Open the project with PyCharm IDE
3. Install all necessary libraries for submission run the following command:
```shell
pip install -r requirements.txt
```

## First Submission
1. Change `<Your Name Here>` on 26th line of `submission_example.py` file to your name
2. Run `submission_example.py` file

## Leaderboard
Visit http://leaderboard.track-debts.com website to see yourself on the leaderboard.
You can see only public leaderboard that evaluated based on 50% of test data and serves as a reference.
The winner will be determined based on private leaderboard that evaluated on another 50% of test data. 
Private Leaderboard will be available 27th of January at 14:30 (Ukrainian timezone).  

## What I can do to win?
Answer: whatever that will improve the accuracy of predictions :)

Just random ideas:
1. Try different ML models mentioned in the following article https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html
2. Feature engineering based on data visualization and analysis
3. Learn and use Deep Learning Networks
4. Convolution Neural Networks should give the best results, but who knows :)
