from sklearn.datasets import load_digits
from sklearn.ensemble import AdaBoostClassifier

from judge_dataset_api import get_test_data, submit_predictions, host

print('Loading train data...')
digits = load_digits()
X_train = digits.data / 16 * 255
y_train = digits.target
print('Train data loaded.')

print('Training a model...')
model = AdaBoostClassifier(n_estimators=1)
model.fit(X_train, y_train)
print('Model trained.')

print('Loading test data...')
X_test = get_test_data() / 16 * 255
print('Test data loaded.')

print('Model predicting...')
y_test_pred = model.predict(X_test)
print('Predicted')

print('Submitting results and receiving score...')
score = submit_predictions(y_test_pred, '<Your Name Here>')
print('Your score is:' + str(score))
print('Visit ' + host + ' website to see yourself on the leaderboard (it shows only test best submission score).')
